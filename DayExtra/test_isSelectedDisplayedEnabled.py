from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

def setup():
#open the first window
 global driver
 driver = webdriver.Chrome()
 driver.maximize_window()


def teardown():
 time.sleep(3)
 driver.quit()

def test_isSelectedDisplayedEnabled():
 driver.get("http://www.google.com")

 isSelected = driver.find_element_by_xpath("//input[@name='q']").is_selected()
 print("\n Is Searchbar selected : ", isSelected)


 isDisplayed = driver.find_element_by_name("q").is_displayed()
 print("\n Is Searchbar Displayed : ", isDisplayed)

 isEnabled = driver.find_element_by_name("q").is_enabled()
 print("\n Is searchbar enabled : ", isEnabled)
