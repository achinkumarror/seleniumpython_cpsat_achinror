import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
driver.maximize_window()
driver.get('https://www.google.com/')

titlestr = driver.title
print("Title of the launched page: " , titlestr, type(titlestr))

time.sleep(4)

#//input[@name='q']
searchbox = driver.find_element_by_xpath("//input[@name='q']")
searchbox.send_keys('Selenium')
searchbox.send_keys(Keys.ENTER)
titlestr1 = driver.title
print("Title of the navigated page: " , titlestr1)

link = driver.find_element_by_xpath("//body[@id='gsr']/div[@id='main']/div[@id='cnt']/div[@class='mw']/div[@id='rcnt']/div[@class='col']/div[@id='center_col']/div[@id='res']/div[@id='search']/div/div[@id='rso']/div[1]/div[1]/div[1]/a[1]/h3[1]")
link.click()
print("Title of the navigated page: " , driver.title)

time.sleep(4)
driver.close()