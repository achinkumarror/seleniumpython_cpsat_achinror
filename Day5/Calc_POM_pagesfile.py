
class ataCalc:

    def __init__(self,driverObj):  # driverObj will come from other method
        self.driver = driverObj   #

    #Object Locators
        self.input1 = self.driver.find_element_by_xpath("//input[@id='ID_nameField1']")
        self.input2 = self.driver.find_element_by_xpath("//input[@id='ID_nameField2']")
        self.addradio = self.driver.find_element_by_xpath("//input[@id='gwt-uid-1']")
        self.mulradio = self.driver.find_element_by_xpath("//*[@id='gwt-uid-2']")
        self.compradio = self.driver.find_element_by_xpath("//input[@id='gwt-uid-4']")
        self.euclidradio = self.driver.find_element_by_xpath("//input[@id='gwt-uid-6']")
        self.calculate_button = self.driver.find_element_by_xpath("//button[@id='ID_calculator']")
        self.result_box = self.driver.find_element_by_xpath("//input[@id='ID_nameField3']")

     #Action Methods()
    def add(self,num1,num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))
        self.input2.clear()
        self.input2.send_keys(str(num2))
        self.addradio.click()
        self.calculate_button.click()

        actRes = int(self.result_box.get_attribute('value'))
        return actRes


    def mul(self,num1,num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))
        self.input2.clear()
        self.input2.send_keys(str(num2))
        self.mulradio.click()
        self.calculate_button.click()

        actRes = int(self.result_box.get_attribute('value'))
        return actRes

    def comp(self,num1,num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))
        self.input2.clear()
        self.input2.send_keys(str(num2))
        self.compradio.click()
        self.calculate_button.click()

        actRes = int(self.result_box.get_attribute('value'))
        return actRes

    def euclid(self,num1,num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))
        self.input2.clear()
        self.input2.send_keys(str(num2))
        self.euclidradio.click()
        self.calculate_button.click()

        actRes = int(self.result_box.get_attribute('value'))
        return actRes