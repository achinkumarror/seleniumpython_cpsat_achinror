import time
from selenium import webdriver
from selenium.webdriver import ActionChains


def setup():
    print("Inside setup method")
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print("Inside teardown method")
    time.sleep(5)
    driver.quit()

def test_calc_mul():
    print("I am inside test method : calc_mul")
    driver.get("http://ata123456789123456789.appspot.com/")

    nbr1 = 10
    nbr2 = 20
    exp_result = 200

    input1 = driver.find_element_by_xpath("//input[@id='ID_nameField1']")
    input1.clear()
    input1.send_keys(str(nbr1))

    input2 = driver.find_element_by_xpath("//input[@id='ID_nameField2']")
    input2.clear()
    input2.send_keys(str(nbr2))

    mulradio = driver.find_element_by_xpath("//*[@id='gwt-uid-2']")
    mulradio.click()

    calculate_button = driver.find_element_by_xpath("//button[@id='ID_calculator']")
    calculate_button.click()

    result_box = driver.find_element_by_xpath("//input[@id='ID_nameField3']")
    actual_result = result_box.get_attribute('value')  #get webelement value(string) from read only box

    assert exp_result == int(actual_result)
    print("Expected Result: " ,exp_result, "matched with the", "Actual Result:" ,actual_result)

#Test Method
def test_calc_add():
    print("I am inside test method : calc_add")
    driver.get("http://ata123456789123456789.appspot.com/")

    nbr1 = 11
    nbr2 = 22
    exp_result = 33

    input1 = driver.find_element_by_xpath("//input[@id='ID_nameField1']")
    input1.clear()
    input1.send_keys(str(nbr1))

    input2 = driver.find_element_by_xpath("//input[@id='ID_nameField2']")
    input2.clear()
    input2.send_keys(str(nbr2))

    addradio = driver.find_element_by_xpath("//input[@id='gwt-uid-1']")
    addradio.click()

    calculate_button = driver.find_element_by_xpath("//button[@id='ID_calculator']")
    calculate_button.click()

    result_box = driver.find_element_by_xpath("//input[@id='ID_nameField3']")
    actual_result = result_box.get_attribute('value')  # get webelement value(string) from read only box

    assert exp_result == int(actual_result)
    print("SUM:" , "Expected Result: ", exp_result, "matched with the", "Actual Result:", actual_result)