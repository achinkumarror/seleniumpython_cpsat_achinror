import time
from selenium import webdriver
from selenium.webdriver import ActionChains
from Day5.Calc_POM_pagesfile import ataCalc  #import class from pagesfile


def setup():
    print("Inside setup method")
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("http://ata123456789123456789.appspot.com/")
    driver.implicitly_wait(10)

def teardown():
    print("Inside teardown method")
    time.sleep(5)
    driver.quit()

#test methods - test cases
def test_calc_add():
    n1 = 11
    n2 = 22
    expRes = 33

    Obj = ataCalc(driver) #Initiate page objects
    actRes = Obj.add(n1,n2) #Refer action methods of page class

    assert expRes == actRes
    print("Expected Result for addition: " ,expRes, "matched with the", "Actual Result:" ,actRes)

def test_calc_mul():
    n1 = 10
    n2 = 20
    expRes = 200

    Obj = ataCalc(driver)  #Initiate page objects
    actRes = Obj.mul(n1,n2) #Refer action methods of page clas

    assert expRes == actRes
    print("Expected Result for Multiply: " ,expRes, "matched with the", "Actual Result:" ,actRes)
