import time
import pytest
from selenium import webdriver
from Day5.Calc_POM_pagesfile import ataCalc  #import class from pagesfile

def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)
    driver.get("http://ata123456789123456789.appspot.com/")

def teardown():
    print('Inside teardown() method')
    time.sleep(2)
    driver.quit()

def dataprovider():
    lol =[  [12,	13,	'add',	25],
            [10,	20,	'Mul',	200],
            [18,	32,	'Add',	50],
            [500,	100,'comp',	500],
            [50,	12,	'add',	62]             ]     # [  [12,13,'Add',27] , [12,13,'Add',27] ,[12,13,'Add',27]  ]
    return lol


@pytest.mark.parametrize('inputdata', dataprovider())
def test_calc_all(inputdata):
    print(inputdata)
    input1 =  inputdata[0]     # 12
    input2 =   inputdata[1]     # 15
    operation =   inputdata[2]     # 'add' # 'Add' 'ADD'
    expRes =   inputdata[3]     # 27
    actRes = ''
    Obj = ataCalc(driver)

    if (operation.upper() == 'ADD'):
        actRes = Obj.add(input1, input2)
    elif  (operation.upper() == 'MUL'):
        actRes = Obj.mul(input1, input2)
    elif (operation.upper() == 'COMP'):
        actRes = Obj.comp(input1, input2)
    else:
        print('Invalid Data Point')



    print(expRes, actRes)
    assert expRes == actRes
