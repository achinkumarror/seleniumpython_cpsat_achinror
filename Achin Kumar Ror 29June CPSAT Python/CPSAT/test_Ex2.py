import time
import pytest
from selenium import webdriver
import pandas
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def dadaGenerator():
    lst = [['Indore'], ['Bhopal'], ['Ujjain']]
    return lst

@pytest.mark.parametrize('data',dadaGenerator())
def test_mptourism(data):
    driver.get("http://www.mptourism.com/")

    time.sleep(2)
    search = driver.find_element_by_xpath("//a[contains(text(),'Search')]")
    search.click()

    searchinput = driver.find_element_by_xpath("//input[@id='keyword']")
    searchinput.send_keys(data)
    searchinput.send_keys(Keys.ENTER)

    #// *[ @ id = "all"] / div[2] / div[2] / div / div / a / h3
    #// *[ @ id = "all"] / div[3] / div[2] / div / div / a / h3

    dest_title = driver.find_elements_by_xpath("//*[@id='all']/div[*]/div[2]/div/div/a/h3")
    print("\n Count of searched items are: " , len(dest_title))

    for i in dest_title:
        print(i.text)

