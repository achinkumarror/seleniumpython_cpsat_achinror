import time
import pytest
from selenium import webdriver
import pandas
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def test_cii():
    driver.get("https://www.cii.in/")

    print(driver.current_url)

    #b. Click on Banking under Economy menu
    #There is no banking under economy
    driver.save_screenshot("../screenshots/cii_banking.png")

    #// *[ @ id = "form1"] / header / div[3] / div[1] / div[1] / a / img
    #// *[ @ id = "form1"] / header / div[3] / div[1] / div[2] / a / img
    #// *[ @ id = "form1"] / header / div[3] / div[1] / div[3] / a / img
    #// *[ @ id = "form1"] / header / div[3] / div[1] / div[4] / a / img

    medialinks = driver.find_elements_by_xpath("//*[@id='form1']/header/div[3]/div[1]/div[*]/a/img")

    for i in medialinks:
        tooltip = i.get_attribute("title")
        print("Tooltips for media icons are: " , tooltip)

    contactus = driver.find_element_by_xpath("//*[@id='form1']/footer/section[1]/aside/ul/li[8]/a")
    contactus.click()
    time.sleep(2)

    actPhone = driver.find_element_by_xpath("//span[contains(text(),'1800 103 1244')]")
    actualPhone = actPhone.text
    assert '1800 103 1244' in actualPhone
    print("assert pass, assert number found")

    time.sleep(2)
    driver.back() # navigate back to hoempage
    time.sleep(2)
    driver.forward() #go forward in the browser

    print(driver.title)
