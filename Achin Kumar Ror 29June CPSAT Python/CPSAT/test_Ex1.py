import time
import pytest
from selenium import webdriver
import pandas
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def test_allmovie():
    driver.get("https://www.allmovie.com/")

    time.sleep(2)
    adv_search = driver.find_element_by_xpath("//li[@class='advanced-search']")
    adv_search.click()

    time.sleep(2)
    chk1 = driver.find_element_by_xpath("//li[@class='genre']//label[contains(text(),'Action')]")
    chk1.click()
    time.sleep(2)
    chk2 = driver.find_element_by_xpath("//li[@class='genre']//label[contains(text(),'Adult')]")
    chk2.click()
    time.sleep(2)
    chk3 = driver.find_element_by_xpath("//li[@class='genre']//label[contains(text(),'Adventure')]")
    chk3.click()

    time.sleep(2)
    query = driver.find_element_by_xpath("//div[@class='query']")
    act = query.text
    print(act)

    assert "Action or Adult or Adventure" in act
    print("assert pass")

    #// *[ @ id = "cmn_wrap"] / div[1] / div[2] / section[2] / div[1] / table / tbody / tr[1] / td[3] / a[1]
    #//*[@id="cmn_wrap"]      /div[1]/div[2]/section[2]/div[1]/table/tbody/tr[2]/td[3]/a[1]
    #// *[ @ id = "cmn_wrap"] / div[1] / div[2] / section[2] / div[1] / table / tbody / tr[3] / td[3] / a[1]
    time.sleep(2)
    title = driver.find_elements_by_xpath("//*[@id='cmn_wrap']/div[1]/div[2]/section[2]/div[1]/table/tbody/tr[*]/td[3]/a[1]")
    for i in title:
        print(i.text)