#3. Using Pytest and WebDriver script in Python , open ‘https://jqueryui.com/checkboxradio/’  in Chrome  and do the below: (10 points)

#a. Check Paris is selected or not, If not then select Paris
#b. Print the checkboxes count that under Checkbox nested in label
#c. Select all checkbox that under Checkbox nested in label
#d. Print the top heading of the page


import time
import pytest
from selenium import webdriver
import pandas
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def test_jqueryui():
    driver.get("https://jqueryui.com/checkboxradio/")

    # Ex5_a
    time.sleep(2)
    seq = driver.find_elements_by_tag_name('iframe')
    iframe = driver.find_elements_by_tag_name('iframe')[0]
    driver.switch_to.frame(iframe)

    time.sleep(2)
    radiobtn2 = driver.find_element_by_xpath("//label[contains(text(),'Paris')]")
    radiobtn2.click()

    #Ex5_b
    #'JB-' | 'ABC'
    #lst_chkbox = driver.find_elements_by_tag_name("/html[1]/body[1]/div[3]/fieldset[3]/label[*]")
    #lst_chkbox = driver.find_elements_by_tag_name("//label[contains(text(),'2 Queen' | '1 Queen' | '1 King' | '2 Double')]")
    #print("Count of checkbox are: " , len(lst_chkbox))
    #for i in lst_chkbox:
     #   print(i.text)

    #Ex5_c
    chk1 = driver.find_element_by_xpath(" //label[contains(text(),'2 Double')]")
    chk1.click()

    chk2 = driver.find_element_by_xpath("//label[contains(text(),'2 Queen')]")
    chk2.click()

    chk3 = driver.find_element_by_xpath("//label[contains(text(),'1 Queen')]")
    chk3.click()

    chk4 = driver.find_element_by_xpath("//label[contains(text(),'1 King')]")
    chk4.click()

    #Ex5_d
    driver.switch_to.default_content()
    time.sleep(2)
    print(driver.title)

