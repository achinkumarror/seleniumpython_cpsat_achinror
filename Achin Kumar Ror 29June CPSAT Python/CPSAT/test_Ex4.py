#	4. Using PyTest and WebDriver script, open https://code.makery.ch/library/dart-drag-and-drop/  in Firefox and perform the following tasks: (12 points)
#a. Scroll till “Custom Drag Avatar”
#b. Drop all the document in the dustbin
#c. Take screenshot before dropping the document in the dustbin


import time
import pytest
from selenium import webdriver
import pandas
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def test_codemakery():
    driver.get("https://code.makery.ch/library/dart-drag-and-drop/")

    #Ex4_a
    action = ActionChains(driver)
    cstmdragavatar = driver.find_element_by_xpath("//*[@id='custom-drag-avatar']")
    #driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
    driver.execute_script("window.scrollTo(0, 1000)")

    # Ex4_b
    #actionChains.drag_and_drop(source, target).perform()
    source = driver.find_elements_by_xpath("/html/body/div[2]/img[*]")
    time.sleep(2)
    target = driver.find_element_by_xpath("/html/body/div[2]/div")
    time.sleep(2)
    action.drag_and_drop(source,target).perform()

    # Ex4_c
    driver.save_screenshot("../screenshots/cokemakery.png")
