import datetime
import time
from typing import List

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def test_letterboxd():
    driver.get("https://letterboxd.com/")

    #Ex5_a
    webelement_logotext = driver.find_element_by_xpath("//a[@class='logo replace']")
    logotext = webelement_logotext.text
    print("\n" , logotext)

    exptext = 'Letterboxd'
    if exptext in logotext:
        print("text found: " , exptext)
    else:
        print("text not found in : " , logotext)

    #Ex5_b
    webelemet_people_link = driver.find_element_by_xpath("//a[contains(text(),'People')]")
    webelemet_people_link.click()
    driver.save_screenshot("../screenshots/people.png")

    #Ex5_c : c. Print the names of the reviewers under the ‘Popular this week’ section In the ‘Popular this
    #week’ section,
    #// *[ @ id = "popular-reviewers"] / ul / li[1] / h3 / a
    #// *[ @ id = "popular-reviewers"] / ul / li[2] / h3 / a
    #// *[ @ id = "popular-reviewers"] / ul / li[3] / h3 / a
    #NOTE: AttributeError: 'list' object has no attribute 'text'

    lol_popularthisweek_names = driver.find_elements_by_xpath("//*[@id='popular-reviewers']/ul/li[*]/h3/a")
    for i in lol_popularthisweek_names:
        print("Popular this week: " , i.text)

    #Ex5_d : d. print the total number of reviews that the first reviewer has

    reviews = driver.find_element_by_xpath("//*[@id='popular-reviewers']/ul/li[1]/p/a[2]")
    totalreviews = reviews.text
    print("\n " , totalreviews)