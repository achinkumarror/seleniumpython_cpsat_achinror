import datetime
import time
from typing import List

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.select import Select


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def test_nseindia():
    #Ex6_ab : a. Go to ‘http://www.nseindia.com/’
    #b. From the dropdown next to the search field, select ‘Currency Derivatives’

    driver.get("https://www1.nseindia.com/index_nse.htm")  #https://www1.nseindia.com/index_nse.htm

    dropdown = driver.find_element_by_xpath("//select[@id='QuoteSearch']")
    s = Select(dropdown)
    s.select_by_index()