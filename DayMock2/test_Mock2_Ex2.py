import datetime
import time
from typing import List

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def test_shopperstop():
    driver.get("https://www.imdb.com/")

    searchbox = driver.find_element_by_xpath("//*[@id='suggestion-search']")
    searchbox.click()
    searchbox.send_keys("Gangs of New York")
    searchbox.send_keys(Keys.ENTER)

    time.sleep(2)
    link = driver.find_element_by_xpath("//*[@id='main']/div/div[2]/table/tbody/tr[1]/td[2]/a")
    link.click()

    time.sleep(2)
    movietime = driver.find_element_by_xpath("//*[@id='title-overview-widget']/div[1]/div[2]/div/div[2]/div[2]/div/time")
    runtime = movietime.text
    print("\n movie time is : " , runtime)

    # how to compare 2h 47 min to 180 min
    rtlist = runtime.strip().split(' ') #split 2h 47min using space
    hr = rtlist[0].split('h')[0]    #split 2h using h
    min = rtlist[1].split('min')[0]     #split 47min using min

    intRunTime = int(hr) * 60 + int(min)   # int(2*60) + int(47)
    print('Runtime in mins is : ', intRunTime)

    assert (intRunTime < 180)
    print("Assert pass movie run time is less than 180")


    time.sleep(2)
    genre = driver.find_element_by_xpath("//*[@id='title-overview-widget']/div[1]/div[2]/div/div[2]/div[2]/div/a[1]")
    print("\n genere of movie contains " , genre.text)


    time.sleep(2)
    MPAArating = driver.find_element_by_xpath("//*[@id='title-overview-widget']/div[1]/div[2]/div/div[2]/div[2]/div")
    rating = MPAArating.text
    print("\n Rating contains R" , rating)

    reviewer = driver.find_element_by_xpath("//*[@id='titleUserReviewsTeaser']/div/span[1]/div[1]/a[1]/span")
    print(" \n Reviwer name is: " , reviewer.text)