import time
import pytest
from selenium import webdriver
from selenium.webdriver.common import keys
from selenium.webdriver.common.keys import Keys
import pandas
from selenium.webdriver.support.select import Select


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(3)
    driver.quit()

def dp_pandas():
    Excel_path = "C:/Users/achin1828/PycharmProjects/pythoncpsat/Resources/Data/Mock2_Ex3.xlsx"
    dataframeObj = pandas.read_excel(Excel_path, 'firstcry')
    list_of_list = dataframeObj.values.tolist()
    return list_of_list

@pytest.mark.parametrize('inputdata',dp_pandas())
def test_firstcry_search(inputdata):
    driver.get("https://www.firstcry.com/")
    searchbox = driver.find_element_by_xpath("//input[@id='search_box']")
    searchbox.clear()
    searchbox.send_keys(inputdata)
    searchbox.send_keys(Keys.ENTER)

    time.sleep(3)
    sortbox = driver.find_element_by_xpath("/html/body/div[4]/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/span[2]")
    sortbox.click()

    time.sleep(2)
    price_link = driver.find_element_by_xpath("//a[@class='na'][contains(text(),'Price')]")
    price_link.click()

# Write script for Google Chrome in PyTest/ WebDriver to test whether the first 8 products are in descending order of the price.
#   // *[ @ id = "maindiv"] / div[1] / div / div[1] / div[3] / span[1] / a
#    // *[ @ id = "maindiv"] / div[2] / div / div[1] / div[3] / span[1] / a
#    // *[ @ id = "maindiv"] / div[3] / div / div[1] / div[3] / span[1] / a
#    // *[ @ id = "maindiv"] / div[4] / div / div[1] / div[3] / span[1] / a

    time.sleep(3)
    price_lol = driver.find_elements_by_xpath("//*[@id='maindiv']/div[*]/div/div[1]/div[3]/span[1]/a")
    first8_price_lol = price_lol[0:8]
    for i in first8_price_lol :
        print("\n" , i.text)

    time.sleep(3)
