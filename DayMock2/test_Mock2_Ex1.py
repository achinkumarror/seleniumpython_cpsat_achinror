import time
from selenium import webdriver


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()
    driver.quit()

def test_shopperstop():
    driver.get("https://www.shoppersstop.com/")

    brandmenu = driver.find_element_by_xpath("//a[contains(text(),'BRANDS')]")
    brandmenu.click()

    time.sleep(4)
    hautecury = driver.find_element_by_xpath("/html/body/main/section/div/div/div/ul/li[1]/div/div[2]/a/div/img")
    hautecury.click()

    driver.save_screenshot("../screenshots/hautecurypage.png")

    time.sleep(2)
    expTitle = "Haute Curry|Shoppers Stop"
    actTitle = driver.title

    assert (expTitle == actTitle)
    print("AssertPass" , expTitle, actTitle)
    #assert expTitle == actTitle

    text = driver.find_element_by_xpath("//p[contains(text(),'start something new')]")
    if text == 'start something new':
        print("Text exists on the page")
    else:
        print("Text does not exists")

   # / html / body / main / footer / div[4] / div[1] / div / div / div / div[3] / ul / li[1] / a
   # / html / body / main / footer / div[4] / div[1] / div / div / div / div[3] / ul / li[2] / a
   # / html / body / main / footer / div[4] / div[1] / div / div / div / div[3] / ul / li[3] / a

    lstoflst_followuslinks = driver.find_elements_by_xpath("/html/body/main /footer/div[4]/div[1]/div/div/div/div[3]/ul/li[*]/a")
    for i in lstoflst_followuslinks:
        print(i.get_attribute("href"))  #get all links with attribute href