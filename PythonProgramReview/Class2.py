from PythonProgramReview.Class1 import Person

class Student(Person):

    def __init__(self,name, age, std):  #defining init of child class  - Student
        super().__init__(name, age)   #calling init of parent class - person
        self.std = std

    def greetings(self):
        print('Greetings from student: ', self.name)


St1 = Student('Rajesh', 14, 'VIII')
print(St1.name,St1.age, St1.std)
St1.sayHello()
St1.greetings()