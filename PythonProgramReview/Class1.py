
class Person:
    def __init__(self, name, age): #self is same as this in Java
        self.name = name
        self.age = age

    def sayHello(self):
        print("Hello Everyone! my name is: ", self.name)

#outside the class
p1 = Person('Achin', 35)  #creating object of class Obj = Class()
print(p1.name,p1.age)
p2 = Person ('Virat',25)
print(p2.name,p2.age)
p3 = Person('Hind',4)

p1.sayHello()
p2.sayHello()
p3.sayHello()