#Ex02B WindowSwitching
#
#Note :  You have to print the title of all the windows which are open
#1. Go to https://www.ataevents.org/
#2. click on trainings to open in new tab

import time
from selenium import webdriver

driver = webdriver.Chrome()
driver.maximize_window()
driver.implicitly_wait(10)
driver.get("https://www.ataevents.org/")

time.sleep(5)
print("tab1")
Tab1 = driver.find_element_by_xpath("//a[contains(text(),'CP-WST Instructor Led Online Program')]")
Tab1.click()

time.sleep(2)
print("tab2")
Tab2 = driver.find_element_by_xpath("//a[contains(text(),'CP-SAT Licensed Trainer Program')]")
Tab2.click()

time.sleep(2)
print("get window handles")
phwh = driver.current_window_handle
lst_wh = driver.window_handles

for handle in lst_wh:
    print(handle)
    driver.switch_to.window()
    print(driver.title)


time.sleep(2)
driver.close()
driver.quit()