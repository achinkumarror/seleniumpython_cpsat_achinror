"""Ex06  : ATASocialMediaHrefJunit (JUnit and TestNG)
1. Go to "http://agiletestingalliance.org/"
2. print hrefs for following social media icons at the footer
    - LinkedIn
	- twitter
	- youtube
	- insta
	- facebook
	- wa
	- Telegram """

from selenium import webdriver

driver = webdriver.Chrome()
driver.maximize_window()
driver.get("http://agiletestingalliance.org/")

lst = driver.find_elements_by_xpath("//*[@id='custom_html-10']//li[*]//a[1]")

for i in lst:
    address = i.get_attribute("href")
    print(address)