"""Ex01 : WiKi Pedia Search
1. Open WiKiPedia.org (Consider it Page1)
2. Click on English
3. Write "Selenium" in Search Box
4. Click on Magnifying Glass
5. Print the title.
6. Verify if the Title is "Selenium - Wikipedia".
"""
import time
from selenium import webdriver

driver = webdriver.Chrome()
driver.maximize_window()
driver.get('https://www.wikipedia.org/')

print("Title of launched page is: ", driver.title)
driver.save_screenshot("../screenshots/wikipag1.png")

langlink = driver.find_element_by_xpath("//strong[contains(text(),'English')]")
langlink.click()
print("Title of navigated page2 is: ", driver.title)
driver.save_screenshot("../screenshots/wikipag2.png")

inputbox = driver.find_element_by_xpath("//input[@id='searchInput']")
inputbox.send_keys("Selenium")

driver.find_element_by_xpath("//input[@id='searchButton']").click()
print("Title of navigated page3 is: ", driver.title)

# assert expected == actual
# assert "Python" in driver.title
# assert "Python" not in driver.title
# assert element.text == 'Example Domains'

assert driver.title == "Selenium - Wikipedia"
print("Title is verified ")

driver.save_screenshot("../screenshots/wikipage3.png")

time.sleep(3)

driver.back()
print("Title of navigated back page4 is: ",driver.title)
driver.forward()
print("Title of navigated forward page5 is: ",driver.title)

time.sleep(3)
driver.close()