import time
import pytest
from selenium import webdriver
import pandas
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def test_qaagility():
    driver.get("https://www.qaagility.com/")

    logo = driver.find_element_by_xpath("//nav[@id='site-navigation']//img[@class='header-image']")
    print("\n Size of logo is: " , logo.size)

    footer = driver.find_element_by_xpath("//div[@class='copyright-bar']")
    actText = footer.text
    print(actText)
    expText = 'QAAgility Technologies © 2020. All Rights Reserved'
    assert (expText == actText)
    print("Assert Pass")

    try:
        twitterlink = driver.find_element_by_xpath("//i[@class='fab fa-twitter']")
    except NoSuchElementException:
        print("No such Element")
        return False
    print("Element Exists")
    return True



