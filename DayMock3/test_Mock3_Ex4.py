import time
import pytest
from selenium import webdriver
import pandas
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def test_allmovie():
    driver.get("https://www.allmovie.com/")
    search = driver.find_element_by_xpath("//input[@placeholder='Search']")
    search.send_keys("God Father")
    search.send_keys(Keys.ENTER)

    time.sleep(2)
    searchresult = driver.find_element_by_xpath("//*[@id='cmn_wrap']/div[1]/div[2]/div/h1")
    print(searchresult.text)

    movlink = driver.find_element_by_xpath("//*[@id='cmn_wrap']/div[1]/div[2]/div/ul/li[2]/div[2]/div[1]/a")
    movlink.click()

    genre = driver.find_element_by_xpath("//*[@id='cmn_wrap']/div[1]/div[2]/header/hgroup[2]/span[1]/a")
    actgenre = genre.text
    expgenre = 'Crime'

    assert (expgenre == actgenre)
    print("Assert pass for genre")

    mpaa = driver.find_element_by_xpath("//*[@id='cmn_wrap']/div[1]/div[2]/header/hgroup[2]/span[6]/span")
    actmpaa = mpaa.text
    expmpaa = 'R'

    assert (expmpaa == actmpaa)
    print("Assert pass for mpaa")

    cctab = driver.find_element_by_xpath("//a[contains(text(),'Cast & Crew')]")
    cctab.click()

    dir = driver.find_element_by_xpath("//*[@id='cmn_wrap']/div[1]/div[2]/section[1]/div[1]/div[1]/div[2]/dl/dt/a")
    actdir = dir.text
    expdir = 'Francis Ford Coppola'

    assert (expdir ==actdir)
    print("assert pass for director")

    cast = driver.find_element_by_xpath("//*[@id='cmn_wrap']/div[1]/div[2]/section[1]/div[2]/div[2]/div[2]/a")
    castname = cast.text

    character = driver.find_element_by_xpath("//div[contains(text(),'Michael Corleone')]")
    actcharname = character.text

    expcharname = 'Michael Corleone'

    if castname == 'Al Pacino':
        assert (expcharname == actcharname)
        print("Al Pacino’s character’s name is: ", actcharname)
    else:
        print("Al Pacino’s character’s name is not matched ")