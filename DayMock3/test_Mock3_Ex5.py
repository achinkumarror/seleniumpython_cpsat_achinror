import time
import pytest
from selenium import webdriver
import pandas
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def test_atacal():
    driver.get("http://ata123456789123456789.appspot.com/")

    firstnbr = driver.find_element_by_xpath("//input[@id='ID_nameField1']")
    firstnbr.clear()
    firstnbr.send_keys("5")

    secondnbr = driver.find_element_by_xpath("//input[@id='ID_nameField2']")
    secondnbr.clear()
    secondnbr.send_keys("2")

    radiobutton = driver.find_element_by_xpath("//input[@id='gwt-uid-6']")
    radiobutton.click()

    calculate = driver.find_element_by_xpath("//button[@id='ID_calculator']")
    calculate.click()

    resultvalue = driver.find_element_by_xpath("//*[@id='ID_nameField3']")
    calvalue = int(resultvalue.get_attribute('value'))

    a= 5
    b= 2
    eqresult = (a * a) + 2 * (a * b) + (b * b)
    print(eqresult)

    assert (eqresult == calvalue)
    print("assert pass as eqvalue match calvalue")