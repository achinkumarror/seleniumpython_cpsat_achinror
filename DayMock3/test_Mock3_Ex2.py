import time
import pytest
from selenium import webdriver
import pandas
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def test_googlemaps():
    driver.get("https://www.google.com/maps/")
    searchbox = driver.find_element_by_xpath("//input[@id='searchboxinput']")
    searchbox.send_keys("Wankhede Stadium")
    searchbox.send_keys(Keys.ENTER)

    driver.save_screenshot("../screenshots/maps_wankhade.png")

    Webelement_text  = driver.find_element_by_xpath("//span[contains(text(),'Wankhede Stadium')]")
    actText = Webelement_text.text
    expText = 'Stadium'

    if expText in actText:
        print("ExpText is available in ActText")
    else:
        print("Text not fund")

    actTitle = driver.title
    expTitle = 'Wankhede Stadium - Google Maps'

    assert (actTitle == expTitle)

    rating = driver.find_element_by_xpath("//*[@id='pane']/div/div[1]/div/div/div[2]/div[1]/div[2]/div/div[1]/span[1]/span/span")
    print(rating.text)

    reviews = driver.find_element_by_xpath("//*[@id='pane']/div/div[1]/div/div/div[2]/div[1]/div[2]/div/div[1]/span[2]/span/span/span[2]/span[1]/button")
    print(reviews.text)

    link = driver.find_element_by_xpath("//div[contains(text(),'mumbaicricket.com')]")
    actLink = link.text
    expLink = 'mumbaicricket.com'
    assert (expLink == actLink)
    print("Assert Pass")

    address = driver.find_element_by_xpath("//*[@id='pane']/div/div[1]/div/div/div[10]/button/div/div[2]/div[1]")
    print(address.text)

    phone = driver.find_element_by_xpath("//div[contains(text(),'022 2279 5500')]")
    actPhone = phone.text
    expPhone = '022 2279 5500'
    assert (expPhone==actPhone)
    print("Assert Pass")

    driver.save_screenshot("../screenshots/phone.png")