import time
from typing import List
import pytest
from selenium import webdriver
import pandas
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def dp_pandas():
    Excel_Path = "C:/Users/achin1828/PycharmProjects/pythoncpsat/Resources/Data/Mock3_Ex1.xlsx"
    dfObj = pandas.read_excel(Excel_Path,'meripustak')
    lol = dfObj.values.tolist()
    return lol

@pytest.mark.parametrize('inputdata', dp_pandas())
def test_meripustak(inputdata):
    driver.get("https://www.meripustak.com/")

    searchbox = driver.find_element_by_xpath("//*[@id='txtsearch']")
    searchbox.clear()
    searchbox.send_keys(inputdata)
    searchbox.send_keys(Keys.ENTER)

    time.sleep(2)
    discountedbooks: List[WebElement] = driver.find_elements_by_xpath("//*[@id='book_list']/ul/li[*]/div[5]")
    top10_discountedbooks = discountedbooks[0:10]
    for i in top10_discountedbooks:
        orignalprice: object = i.text
        #print(orignalprice)

    time.sleep(1)
    discountedprice: List[WebElement] = driver.find_elements_by_xpath("//*[@id='book_list']/ul/li[*]/div[6]/span[2]")
    top10_discountedprice = discountedprice[0:10]
    for i in top10_discountedprice:
        offprice: object = i.text
        #print(offprice)

        if orignalprice > offprice:
            print("Price after OFF is: " , offprice)
        else:
            print("No Discount on this book", orignalprice)
