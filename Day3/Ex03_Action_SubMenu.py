"""Ex03 : Exercise on Anna University

1. Go to http://www.annauniv.edu/department/index.php

2. Hover on Civil Engineering
3. Click on Institute of ocean management
4. Print the title

What is EventListeners ? -->
How to remove extra event listeners --> expand --> remove
#ActionChains
"""
from selenium import webdriver
from selenium.webdriver import ActionChains

driver = webdriver.Chrome()
driver.maximize_window()
driver.implicitly_wait(10)
driver.get("http://www.annauniv.edu/department/index.php")

#xpath for Civil Eng link - //strong[contains(text(),'Faculty of Civil Engineering')]
#xpath for link IOM  - //div[@id='menuItemHilite32']

action = ActionChains(driver)
menu = driver.find_element_by_xpath("//strong[contains(text(),'Faculty of Civil Engineering')]")
action.move_to_element(menu).perform()
#action.click_and_hold()
#action.double_click()
#action.context_click()
#action.drag_and_drop()
#action.drag_and_drop_by_offset()
#action.key_up()
#action.key_down()
action.move_by_offset()
action.move_to_element_with_offset()
action.release()
action.send_keys()
action.send_keys_to_element()



menulink = driver.find_element_by_xpath("//div[@id='menuItemHilite32']")
action.move_to_element(menulink).perform()
menulink.click()

#OR
#action.move_to_element(menu).move_to_element(menulink).click().perform()

print(driver.title)

driver.close()