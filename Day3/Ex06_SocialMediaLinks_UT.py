import unittest
from selenium import webdriver


class MyTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Entered Setup()")
        global driver
        driver = webdriver.Chrome()
        driver.maximize_window()

    @classmethod
    def tearDownClass(cls):
        print("Entered tearDown()")
        driver.quit()

    def test_Href(self):
        print("Entered test1")
        driver.get("http://agiletestingalliance.org/")

        lst = driver.find_elements_by_xpath("//*[@id='custom_html-10']//li[*]//a[1]")

        for i in lst:
            address = i.get_attribute("href")
            print(address)

    def test2_Href(self):
        print("Entered test2")
        driver.get("http://agiletestingalliance.org/")

        lst = driver.find_elements_by_xpath("//*[@id='custom_html-10']//li[*]//a[1]")

        for i in lst:
            address = i.get_attribute("href")
            print(i, address)



if __name__ == '__main__':
    unittest.main()
