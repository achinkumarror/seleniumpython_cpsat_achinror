# Running ex 03 (clicking of submenu) using pytest
# Use of action class
#

from selenium import webdriver
from selenium.webdriver import ActionChains


def setup():
    print("Inside setup method")
    global driver
    driver = webdriver.Chrome()
    driver.get("https://www.annauniv.edu/department/index.php")
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print("Inside teardown method")
    driver.quit()

def test_validate_iom():
    print("I am inside test method : test_validate_iom()")
    action = ActionChains(driver)
    menu = driver.find_element_by_xpath("//strong[contains(text(),'Faculty of Civil Engineering')]")
    menulink = driver.find_element_by_xpath("//div[@id='menuItemHilite32']")
    action.move_to_element(menu).move_to_element(menulink).click().perform()
    print(driver.title)