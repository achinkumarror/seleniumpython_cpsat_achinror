import time

from selenium import webdriver


def setup():
    print("Inside setup method - Launching Browser")
    global driver
    driver = webdriver.Chrome()
    driver.get("http://the-internet.herokuapp.com/javascript_alerts")
    driver.maximize_window()
    driver.implicitly_wait(10)


def teardown():
    print("Inside teardown method - Closing Browser")
    time.sleep(4)
    driver.quit()


def test_validate_js_alert():
    print("I am inside JS Alert test method")
    first_alert = driver.find_element_by_xpath("//button[contains(text(),'Click for JS Alert')]")
    first_alert.click()

    alert1 = driver.switch_to.alert
    alert1.accept()
    expResult = "You successfuly clicked an alert"

    actResult = driver.find_element_by_xpath("//p[@id='result']").text

    if (expResult == actResult):
        print("You successfuly clicked an alert")
    else:
        print("FAILED - Not able to click on Alert")

def test_validate_js_confirm():
    print("I am inside JS Alert test method")
    second_alert = driver.find_element_by_xpath(" //button[contains(text(),'Click for JS Confirm')]")
    second_alert.click()

    alert2 = driver.switch_to.alert
    #alert2.accept()
    #expResult = "You clicked: Ok"

    alert2.dismiss()
    expResult = "You clicked: Cancel"

    actResult = driver.find_element_by_xpath("//p[@id='result']").text

    assert expResult ==actResult
    print("You clicked on Alert 2")

def test_validate_js_prompt():
    print("I am inside JS Alert test method")
    third_alert = driver.find_element_by_xpath("//button[contains(text(),'Click for JS Prompt')]")
    third_alert.click()

    alert3 = driver.switch_to.alert
    alert3.send_keys()