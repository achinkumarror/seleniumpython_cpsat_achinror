"""
Ex11 : JQuery example - UIFrame
1. go to "https://jqueryui.com/autocomplete/"
2. Type J in the textbox next to Tags
3. It will autocomplete list.
4. Print all the list items
5. select Java
"""
"""import time
from selenium import webdriver
from selenium.webdriver.support.select import Select

def setup():
    print("Inside setup method")
    global driver
    driver = webdriver.Chrome()
    driver.get("https://jqueryui.com/autocomplete/")
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print("Inside teardown method")
    time.sleep(3)
    driver.quit()

def test_verify_jQuery_iframe():"""
