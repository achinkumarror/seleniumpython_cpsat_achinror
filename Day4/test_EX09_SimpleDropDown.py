"""
Ex09 : Sample Drop Down Exercise
1. copy dropdown.html file in resources/data folder
2. select CP-AAT as the selected option
3. Print all the options available
4. change the selection to CP-MLDS
"""
import time
from selenium import webdriver
from selenium.webdriver.support.select import Select

def setup():
    print("Inside setup method")
    global driver
    driver = webdriver.Chrome()
    #driver.get("https://www.annauniv.edu/department/index.php")
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print("Inside teardown method")
    time.sleep(3)
    driver.quit()

def test_verify_dropdown():
    print("Inside test dropdown method")
    driver.get("file:///C:/Users/achin1828/PycharmProjects/pythoncpsat/Resources/Data/dropdown.html")

    dropdown = driver.find_element_by_xpath("/html/body/form/select")
    selectObj = Select(dropdown)
    optionlist = selectObj.options

    for i in optionlist:
        print(i.text)

    #selectObj.select_by_visible_text("xyz")
    #selectObj.select_by_value("3")
    #selectObj.select_by_index(2)


    time.sleep(3)
    selectObj.select_by_visible_text("CP-AAT")
    time.sleep(3)
    selectObj.select_by_value("3")
    time.sleep(3)
    selectObj.select_by_index(0)