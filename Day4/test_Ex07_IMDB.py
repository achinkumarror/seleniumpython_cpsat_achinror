"""Ex07 : IMDB Exercise
TASK#1
1. Go To https://www.imdb.com/
2. Search for movie Raazi
3. Click on magnifying glass
4. On new page click on the first link for Raazi
5. On the next page confirm
Director: Meghna Gulzar
Stars: Vicky Kaushal

Task#2 Repeat the same for
102 Not Out
Director as "Umesh Shukla"
Star Name is "Rishi Kapoor"

Task#3 Repeat the same for
Baazigar
Director as "Mastan"
Star Name is "Shah Rukh Khan"

Task#4 Repeat the same for
Uri
Director: Aditya Dhar
Stars: Vicky Kaushal
"""

import time
import pytest
from selenium import webdriver
from selenium.webdriver.support.select import Select


def setup():
    print("Inside setup method")
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print("Inside teardown method")
    time.sleep(3)
    driver.quit()

def dadaGenerator():
    lst = [['Raazi','Meghna Gulzaar', 'Vicky Kaushal'], ['Baazigar','Mastan','Kajol'], ['URI','Aditya', 'Vicky Kaushal']]
    return lst

@pytest.mark.parametrize('data',dadaGenerator())
def test_verify_imdb_search(data):
    movie = data[0]
    expDirector = data[1]
    expStar = data[2]
    print(movie,expDirector,expStar)
    print(data)

    driver.get("https://www.imdb.com/")

    search_box = driver.find_element_by_xpath("//*[@id='suggestion-search']")
    search_box.send_keys(movie)

    search_icon = driver.find_element_by_xpath("// *[@id='suggestion-search-button']")
    search_icon.click()

    movie_name = driver.find_element_by_xpath("//*[@id='main']/div/div[2]/table/tbody/tr[1]/td[2]/a")
    movie_name.click()

    #//*[@id="title-overview-widget"]/div[2]/div[1]/div[4]/h4
    #"//*[@class='inline' and contains(text(), 'Stars')]/following-sibling::* "
    #xpath, contains() , siblings, find_elements

    lst_stars = driver.find_elements_by_xpath("//*[@class='inline' and contains(text(),'Stars')]/following-sibling::* ")

    for s in lst_stars:
        print("Stars Name: ", s.text)
        if expStar in s.text:
            print("found the star in the list", expStar, s.text)
            break

