import time
import pytest
from selenium import webdriver
from selenium.webdriver.common import keys
from selenium.webdriver.common.keys import Keys
import pandas
from selenium.webdriver.support.select import Select


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(2)
    driver.quit()

def dp_pandas():
    Excel_path = "C:/Users/achin1828/PycharmProjects/pythoncpsat/Resources/Data/Mock1_Ex1.xlsx"
    dataframeObj = pandas.read_excel(Excel_path, 'Pepperfry')
    list_of_list = dataframeObj.values.tolist()
    return list_of_list

@pytest.mark.parametrize('inputdata',dp_pandas())
def test_pepperfry_search(inputdata):
    driver.get("https://www.pepperfry.com/")
    searchbox = driver.find_element_by_xpath("//*[@id='search']")
    searchbox.clear()
    searchbox.send_keys(inputdata)
    searchbox.send_keys(Keys.ENTER)

    time.sleep(2)
    sortbox = driver.find_element_by_xpath("//*[@id='curSortType']")
    sortbox.click()

    #//*[@id="sortBY"]/li[2]/a
    time.sleep(5)
    sortval = driver.find_element_by_xpath("//*[@id='sortBY']/li[2]/a")
    sortval.click()

    price = driver.find_elements_by_xpath("//*[@id='p_*_1_1831150']/div/div[5]/div/span[1]")
    for i in price:
        offerprice = i.text
        print(offerprice)


