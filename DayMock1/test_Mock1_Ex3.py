import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(3)
    driver.quit()

def test_maps_google():
    driver.get("https://www.google.co.in/maps")

    searchbox = driver.find_element_by_xpath("//input[@id='searchboxinput']")
    searchbox.send_keys("Samridhi Grand Avenue, Techzone 4 ")
    searchbox.send_keys(Keys.ENTER)
    time.sleep(5)

    address = driver.find_element_by_xpath("//span[contains(text(),'Plot No. GH09D, West, U.P')]")
    address.text
    print( " \n " , address.text)
    driver.save_screenshot("../screenshots/googlemaps1.png")

    direction = driver.find_element_by_xpath("//*[@id='pane']/div/div[1]/div/div/div[4]/div[1]/div[1]/div[1]/div[3]/div[2]/button/div[1]")
    direction.click()

    from_location = driver.find_element_by_xpath("//*[@id='sb_ifc51']/input")
    from_location.clear()
    from_location.send_keys("Nagarro Software Noida")
    from_location.send_keys(Keys.ENTER)
    time.sleep(3)

    distance = driver.find_element_by_xpath("//*[@id='section-directions-trip-0']/div[2]/div[1]/div[1]/div[2]")
    print('distance calculated: ', distance.text)
    time_cal = driver.find_element_by_xpath("//*[@id='section-directions-trip-0']/div[2]/div[1]/div[1]/div[1]/span[1]")
    print('time calculated: ' , time_cal.text)