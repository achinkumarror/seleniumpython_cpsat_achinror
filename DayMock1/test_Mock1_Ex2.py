import time

from selenium import webdriver
from selenium.webdriver.common import keys
from selenium.webdriver.common.keys import Keys


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)


def teardown():
    time.sleep(1)
    driver.quit()


def test_wiki_org():
    driver.get("https://www.wikipedia.org/")
    text = driver.find_element_by_xpath("//*[@id='js-link-box-en']/small/bdi").text
    print(" \n Number of article in English : ", text)

    searchbox = driver.find_element_by_xpath("//input[@id='searchInput']")
    searchbox.send_keys("Anna University")
    searchbox.send_keys(Keys.ENTER)

    motto = driver.find_element_by_xpath("//td[contains(text(),'Progress Through Knowledge')]").text
    print("\n Motto in English : " , motto)

    expText = 'Knowledge'
    if expText in motto:
        print("Success")
    else:
        print("Text not found")