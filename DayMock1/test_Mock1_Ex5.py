import time

from selenium import webdriver


def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    time.sleep(5)
    driver.quit()

def test_muacin():
    driver.get("https://mu.ac.in/science-technology")
    doit = driver.find_element_by_xpath("//*[@id='post-2758']/div/div[4]/div[3]/div/div/div/p/a")
    doit.click()