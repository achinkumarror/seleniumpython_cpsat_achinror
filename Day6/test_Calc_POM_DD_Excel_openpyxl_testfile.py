import time
import openpyxl as openpyxl  #openpyxl is used to Read excel
import pytest
from selenium import webdriver
from Day5.Calc_POM_pagesfile import ataCalc  #import class from pagesfile


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)
    driver.get("http://ata123456789123456789.appspot.com/")

def teardown():
    print('Inside teardown() method')
    time.sleep(2)
    driver.quit()

def dataprovider():

    Excel_File_path = "C:/Users/achin1828/PycharmProjects/pythoncpsat/Resources/Data/atacalcdata.xlsx"

    workbook = openpyxl.load_workbook(Excel_File_path)  # read excel at this path
    sheet = workbook['calcdata']    #read sheet with this name from workbook
    nbr_of_inputrows = sheet.max_row     #read these rows from this sheet

    print("Number of rows with inputdata in sheet: " , nbr_of_inputrows)

    list_of_list = []   #list of list to store multiple rows [[row1],[row2],[row3]]
    for i in range(2,nbr_of_inputrows+1):   # Note - last index(nbr) in range is excluded, start from 2 to exclude header row data
        var1 = sheet.cell(i,1)    # i is number of rows, 1 - is column number, excel start from 1st row and column.it has no zero
        var2 = sheet.cell(i,2)
        var3 = sheet.cell(i,3)
        var4 = sheet.cell(i,4)

        lst = []                #Empty List, Note- List start with index zero [var1.value,var2.value,var3.value,var4.value]
        lst.insert(0, var1.value)
        lst.insert(1, var2.value)
        lst.insert(2, var3.value)
        lst.insert(3, var4.value)

        list_of_list.insert(i-1,lst) #i is row of excel starts from 1 but list starts from 0 so, i-1

    return list_of_list


@pytest.mark.parametrize('inputdata', dataprovider())
def test_calc_all(inputdata):
    print(inputdata)
    input1 =  inputdata[0]     # 12
    input2 =   inputdata[1]     # 15
    operation =   inputdata[2]     # 'add' # 'Add' 'ADD'
    expRes =   inputdata[3]     # 27

    actRes = ''
    Obj = ataCalc(driver) #Initiating object of page class

    if (operation.upper() == 'ADD'):
        actRes = Obj.add(input1, input2)
    elif  (operation.upper() == 'MUL'):
        actRes = Obj.mul(input1, input2)
    elif (operation.upper() == 'COMP'):
        actRes = Obj.comp(input1, input2)
    elif (operation.upper() == 'EUCLID'):
        actRes = Obj.euclid(input1, input2)
    else:
        print('Invalid Data Point')



    print(expRes, actRes)
    assert actRes == expRes


