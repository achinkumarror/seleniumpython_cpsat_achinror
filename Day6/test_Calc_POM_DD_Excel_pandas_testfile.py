import time
import pytest
from selenium import webdriver
from Day5.Calc_POM_pagesfile import ataCalc  #import class from pagesfile
import pandas as pd


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)
    driver.get("http://ata123456789123456789.appspot.com/")

def teardown():
    print('Inside teardown() method')
    time.sleep(2)
    driver.quit()


def dp_pandas():
    Excel_File_path = "C:/Users/achin1828/PycharmProjects/pythoncpsat/Resources/Data/atacalcdata.xlsx"
    dataframe_Obj = pd.read_excel(Excel_File_path, 'calcdata')
    list_of_list = dataframe_Obj.values.tolist()
    return list_of_list


@pytest.mark.parametrize('inputdata', dp_pandas())
def test_calc_all(inputdata):
    print(inputdata)
    input1 =  inputdata[0]
    input2 =   inputdata[1]
    operation =   inputdata[2]     # 'add' # 'Add' 'ADD'
    expRes =   inputdata[3]

    actRes = ''
    Obj = ataCalc(driver) #Initiating object of page class

    if (operation.upper() == 'ADD'):
        actRes = Obj.add(input1, input2)
    elif  (operation.upper() == 'MUL'):
        actRes = Obj.mul(input1, input2)
    elif (operation.upper() == 'COMP'):
        actRes = Obj.comp(input1, input2)
    elif (operation.upper() == 'EUCLID'):
        actRes = Obj.euclid(input1, input2)
    else:
        print('Invalid Data Point')



    print(expRes, actRes)
    assert actRes == expRes


